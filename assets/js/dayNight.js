const dayNight = document.querySelector(".dayNight");

dayNight.addEventListener("click", () => {
    const body = document.body;
    if(body.classList.contains("dark")){
        body.classList.remove("dark");
        body.classList.add("light");
        dayNight.innerHTML = "Mode 🌒"
    } else if(body.classList.contains("light")){
        body.classList.remove("light");
        body.classList.add("dark");
        dayNight.innerHTML = "mode ☀️"
    }
});    