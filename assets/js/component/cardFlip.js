const cards = document.querySelectorAll('.card');
const restartButton = document.getElementById('restartButton');
const reversoMsg = document.querySelector('.reversoMsg');
const score = document.getElementById('score');
let flippedCards = [];
let matchedCards = [];
let scoreCount = 0;

cards.forEach(card => {
  card.addEventListener('click', function() {
    if (canFlipCard(card)) {
      flipCard(card);
      if (flippedCards.length === 2) {
        checkForMatch();
      }
    }
  });
});

function canFlipCard(card) {
  return !card.classList.contains('flipped') && flippedCards.length < 2 && !matchedCards.includes(card);
}

function flipCard(card) {
  card.classList.add('flipped');
  flippedCards.push(card);
}

function checkForMatch() {
  const card1 = flippedCards[0];
  const card2 = flippedCards[1];
  const name1 = card1.getAttribute('name');
  const name2 = card2.getAttribute('name');

  if (name1 === name2) {
    matchedCards.push(card1, card2);
    scoreCount++;
    score.innerHTML = scoreCount;
    if (checkGameEnd()) {
        // add innerHTML to of victory message to reversoMsg
        score.innerHTML = 'Victoire !';
        reversoMsg.innerHTML = 'Félicitations ! Vous avez terminé le jeu.';
    } 
    else {
        reversoMsg.innerHTML = 'Vous avez trouvé une paire !';
        setTimeout(() => {
            reversoMsg.innerHTML = '[ ]';
          }, 2000);
      }
  } else {
    reversoMsg.innerHTML = 'Ce n\'est pas une paire !';
    setTimeout(() => {
      card1.classList.remove('flipped');
      card2.classList.remove('flipped');
    }, 500);
  }

  flippedCards = [];
}

function checkGameEnd() {
  return matchedCards.length === cards.length;
}

restartButton.addEventListener('click', restartGame);
  
function restartGame() {
    const cardsContainer = document.querySelector('.dis-center-wrap');
    const cards = Array.from(document.querySelectorAll('.card'));
    
    // Réinitialiser les cartes retournées et correspondantes
    flippedCards = [];
    matchedCards = [];
    reversoMsg.innerHTML = '[ ]';
    score.innerHTML = '0';
    scoreCount = 0;

        // Retirer la classe "flipped" de toutes les cartes
    cards.forEach(card => {
      card.classList.remove('flipped');
    });
    
    // Retirer les cartes du DOM
    cards.forEach(card => {
      cardsContainer.removeChild(card);
    });
    
    // Mélanger les cartes
    shuffle(cards);
    
    // Ajouter les cartes mélangées au DOM avec un délai entre chaque carte
    cards.forEach((card, index) => {
      setTimeout(() => {
        cardsContainer.appendChild(card);
      }, 250 * index);
    });
    
    // Réinitialiser le score
    score.innerHTML = '0';
  }
  
  function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }









// const cards = document.querySelectorAll('.card');

// cards.forEach(card => {
//     card.addEventListener( 'click', function() {
//         card.classList.toggle('flipped');
//     });
// });




// const cards = document.querySelectorAll('.card');
// let flippedCards = [];
// let matchedCards = [];

// cards.forEach(card => {
//   card.addEventListener('click', function() {
//     if (!card.classList.contains('flipped') && flippedCards.length < 2) {
//       card.classList.add('flipped');
//       flippedCards.push(card);

//       if (flippedCards.length === 2) {
//         checkForMatch();
//       }
//     }
//   });
// });

// function checkForMatch() {
//   const card1 = flippedCards[0];
//   const card2 = flippedCards[1];
//   const name1 = card1.getAttribute('name');
//   const name2 = card2.getAttribute('name');

//   if (name1 === name2) {
//     card1.removeEventListener('click', flipCard);
//     card2.removeEventListener('click', flipCard);
//     matchedCards.push(card1, card2);
//     checkGameEnd();
//   } else {
//     setTimeout(() => {
//       card1.classList.remove('flipped');
//       card2.classList.remove('flipped');
//     }, 1000);
//   }

//   flippedCards = [];
// }

// function checkGameEnd() {
//   if (matchedCards.length === cards.length) {
//     console.log('Félicitations ! Vous avez terminé le jeu.');
//   }
// }
