          // Select all buttons with the "butn-cloud" class
          const buttons = document.querySelectorAll('.butn-cloud');
          console.log('buttons cloud');
          // Define a function to randomly replace a button's class with "butn-glow" for 1 second
          function highlightRandomButton() {
          // Select a random button from the list of buttons
          const randomButton = buttons[Math.floor(Math.random() * buttons.length)];
          // Replace the "butn-out" class with "butn-glow" on the selected button
          randomButton.classList.replace('butn-out', 'butn-glow');
          // Remove the "butn-glow" class after 2 seconds
          setTimeout(() => {
              randomButton.classList.replace('butn-glow', 'butn-out');
          }, 2000);
          }
  
          // Call the highlightRandomButton function every 1 second
          setInterval(highlightRandomButton, 2000);