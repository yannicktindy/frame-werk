<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComponentCssController extends AbstractController
{
    #[Route('/component/css', name: 'app_component_css')]
    public function index(): Response
    {
        return $this->render('component_css/index.html.twig', [
            'controller_name' => 'ComponentCssController',
        ]);
    }

    #[Route('/component/css/palette', name: 'app_component_css_palette')]
    public function palette(): Response
    {
        return $this->render('component_css/palette.html.twig', [
            'controller_name' => 'ComponentCssController',
        ]);
    }

    #[Route('/component/css/button', name: 'app_component_css_button')]
    public function button(): Response
    {
        return $this->render('component_css/button.html.twig', [
            'controller_name' => 'ComponentCssController',
        ]);
    }   
    
    #[Route('/component/css/font', name: 'app_component_css_font')]
    public function font(): Response
    {
        return $this->render('component_css/font.html.twig', [
            'controller_name' => 'ComponentCssController',
        ]);
    } 

    #[Route('/component/css/margin', name: 'app_component_css_margin')]
    public function margin(): Response
    {
        return $this->render('component_css/margin.html.twig', [
            'controller_name' => 'ComponentCssController',
        ]);
    } 


}
