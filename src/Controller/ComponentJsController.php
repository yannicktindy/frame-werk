<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ComponentJsController extends AbstractController
{
    #[Route('/component/js', name: 'app_component_js')]
    public function index(): Response
    {
        return $this->render('component_js/index.html.twig', [
            'controller_name' => 'ComponentJsController',
        ]);
    }

    #[Route('/component/js/btncloud', name: 'app_component_js_btncloud')]
    public function btncloud(): Response
    {
        return $this->render('component_js/butncloud.html.twig', [
            'controller_name' => 'ComponentJsController',
        ]);
    }
}
