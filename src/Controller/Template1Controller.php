<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Template1Controller extends AbstractController
{
    #[Route('/t1', name: 'app_t1')]
    public function index(): Response
    {
        return $this->render('template1/t1.html.twig', [
            'controller_name' => 'Template1Controller',
        ]);
    }
}
